ansible-role-xfs-ftype
======================

A brief description of the role goes here.
This role is generating an ansible-compatible inventory to find out
which nodes have XFS filesystem and which ones have ftype=0.

Requirements
------------

None.

Role Variables
--------------

None.

Dependencies
------------

None.

Generate Inventory
------------------

Generates an ansible-compatible inventory grouped by "xfs-ftype-0", "xfs-ftype-1" and
"not-xfs" hosts at /tmp/filesystem-inventory.

Example::

    $ ansible-playbook -i <inventory> get-inventory.yml
    ...
    $ cat /tmp/filesystem-inventory
    [xfs-ftype-0]
    overcloud-compute1 state=xfs-ftype-0 fqdn=overcloud-compute1.mydomain
    overcloud-compute2 state=xfs-ftype-0 fqdn=overcloud-compute2.mydomain
    [xfs-ftype-1]
    overcloud-compute3 state=xfs-ftype-1 fqdn=overcloud-compute3.mydomain
    overcloud-compute4 state=xfs-ftype-1 fqdn=overcloud-compute4.mydomain
    [not-xfs]
    overcloud-compute5 state=not-xfs fqdn=overcloud-compute5.mydomain
    overcloud-compute6 state=not-xfs fqdn=overcloud-compute6.mydomain

License
-------

Apache 2.0

TODO
----

* Create tasks to format disks with the right XFS configuration

Thanks
------

I want to thank @dmsimard for his role which inspired me:
https://github.com/dmsimard/ansible-meltdown-spectre-inventory/
